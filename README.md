# webpack4-node18-demo

This is a small demo to isolate and allow for better understanding of the Node.js v18 issue found with React, Babel and
Webpack.

## Problem

Webpack and its plugin and loaders are using the `md4` hashing algorithm provided by Node.js by default.
This now outdated algorithm was removed with Node.js v17.

## Libraries using 'md4'

* compression-webpack-plugin v6.1.1
* file-loader v6.2.0
* css-loader v5.2.7

## Setup

To get started, use nvm to install Node:

```shell
nvm install
nvm use
````

Install the dependencies:

```shell
npm install
````

When issuing a build, the following crypto error is shown if no md4 hashing algorithm is available.

```shell
npm run build
```

```log
➜  webpack4-node18-demo git:(main) ✗ npm run build

> webpack4-node18-demo@1.0.0 build
> webpack --mode production

node:internal/crypto/hash:71
  this[kHandle] = new _Hash(algorithm, xofLen);
                  ^

Error: error:0308010C:digital envelope routines::unsupported
    at new Hash (node:internal/crypto/hash:71:19)
    at Object.createHash (node:crypto:133:10)
    at module.exports (/home/olof/git/webpack4-node18-demo/node_modules/webpack/lib/util/createHash.js:135:53)
    at NormalModule._initBuildHash (/home/olof/git/webpack4-node18-demo/node_modules/webpack/lib/NormalModule.js:417:16)
    at handleParseError (/home/olof/git/webpack4-node18-demo/node_modules/webpack/lib/NormalModule.js:471:10)
    at /home/olof/git/webpack4-node18-demo/node_modules/webpack/lib/NormalModule.js:503:5
    at /home/olof/git/webpack4-node18-demo/node_modules/webpack/lib/NormalModule.js:358:12
    at /home/olof/git/webpack4-node18-demo/node_modules/loader-runner/lib/LoaderRunner.js:373:3
    at iterateNormalLoaders (/home/olof/git/webpack4-node18-demo/node_modules/loader-runner/lib/LoaderRunner.js:214:10)
    at iterateNormalLoaders (/home/olof/git/webpack4-node18-demo/node_modules/loader-runner/lib/LoaderRunner.js:221:10)
    at /home/olof/git/webpack4-node18-demo/node_modules/loader-runner/lib/LoaderRunner.js:236:3
    at context.callback (/home/olof/git/webpack4-node18-demo/node_modules/loader-runner/lib/LoaderRunner.js:111:13)
    at /home/olof/git/webpack4-node18-demo/node_modules/babel-loader/lib/index.js:59:71 {
  opensslErrorStack: [ 'error:03000086:digital envelope routines::initialization error' ],
  library: 'digital envelope routines',
  reason: 'unsupported',
  code: 'ERR_OSSL_EVP_UNSUPPORTED'
}

Node.js v18.12.1
➜  webpack4-node18-demo git:(main) ✗ 
```

## Resources

* [webpack.js.org: Output](https://v4.webpack.js.org/configuration/output/#outputhashfunction)
* [webpack.js.org: file-loader](https://v4.webpack.js.org/loaders/file-loader/#hashtype)